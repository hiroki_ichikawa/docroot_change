/**
 * jquery.designedform.js
 * フォームにデザインを入れる
 * 
 *     <input rel="aaa">
 *     <div id="aaa">
 * 
 * この２つを用意して
 * 
 *     $('input').designedForm();
 * 
 * とすると、<div>のデザインになったように見える
 * 
 *     <input rel="bbb">
 *     <div id="bbb" class="narrow">
 * 
 * とすると、テキストに長体がかかる
 * 
 * 
 * v.1.0.0
 * 2013.04.01
 */


;(function($){
	
	
	$.fn.extend({
		
		designedForm : function(){
			
			return $(this).each(function(){
				
				var df = new DesignedForm($(this));
				df.build();
				
			});
			
		}
		
	});
	
	
	/**
	 * メインクラス
	 * @item {jQuery} 要素のjQueryオブジェクト
	 */
	function DesignedForm(item){
		
		// 親要素
		this.parent = item.parent();
		
		// フォーム要素
		this.item = item;
		
		// id
		this.id = item.attr('rel');
		
		// CSSが適用されてる要素
		this.des = $('#' + this.id);
		
		// ラッパ
		this.div = $('<div class="designed-form-wrapper">');
		
		// 長体用のラッパ
		this.span = $('<span>').css({ whiteSpace : 'nowrap' });
		
		// 長体にするかどうか
		this.isNarrow = (this.des.hasClass('narrow')) ? true : false ;
		
	}
	DesignedForm.prototype = {
		
		/**
		 * 組み立て
		 */
		build : function(){
			var A = this;
			
			// select
			if(this.item.get(0).nodeName.toLowerCase() == 'select'){
				this.selectInit();
			}
			// button
			else if(this.item.get(0).nodeName.toLowerCase() == 'button'){
				this.buttonInit();
			}
			// input
			else if(this.item.get(0).nodeName.toLowerCase() == 'input'){
				// radio
				if(this.item.attr('type').toLowerCase() == 'radio'){
					this.radioInit();
				}
				// checkbox
				if(this.item.attr('type').toLowerCase() == 'checkbox'){
					this.checkInit();
				}
				// submit
				else if(this.item.attr('type').toLowerCase() == 'submit'){
					this.submitInit();
				}
			}
			
			this.item.hover(function(){
				A.des.addClass('designed-form-hover');
			},function(){
				A.des.removeClass('designed-form-hover');
			});
			
		},
		
		/**
		 * submitの場合
		 */
		submitInit : function(){
			var A = this;
			
			// 大きさ
			var w = this.des.width() + (parseInt(this.des.css('padding-left')) || 0) + (parseInt(this.des.css('padding-right')) || 0);
			var h = this.des.height() + (parseInt(this.des.css('padding-top')) || 0) + (parseInt(this.des.css('padding-bottom')) || 0);
			
			// ラッパのスタイル
			this.div.css({
				width : w,
				height : h,
				display : 'inline-block',
				position : 'relative',
				verticalAlign : 'middle'
			});
			
			// IE7対応
			if(!$.support.tbody){
				this.div.css({
					display : 'inline',
					zoom : 1
				});
			}
			
			// 余分なスタイルを剥がす
			this.item.css({
				position : 'absolute',
				left : 0,
				top : 0,
				width : w,
				height : h,
				margin : 0,
				padding : 0,
				opacity : 0,
				zIndex : 5,
				cursor : 'pointer'
			});
			
			// フォーム要素はラップする
			this.item.wrap(this.div);
			this.div = this.item.parent();
			
			// CSSが適用されてる要素を一旦外す
			this.des.remove();
			this.des.css({ position : 'absolute', left : 0, top : 0 });
			
			// 長体の処理
			if(!!this.isNarrow){
				this.span.html(this.item.val());
				this.des.append(this.span);
				this.div.prepend(this.des);
				this.span.strNarrow(this.des.width());
			}
			else{
				this.des.html(this.item.val());
				this.div.prepend(this.des);
			}
			
			this.des.addClass('des');
			
			if(this.item.is(':disabled')){
				this.des.addClass('disabled');
				this.item.css({
					cursor : 'default'
				});
			}
		},
		
		/**
		 * buttonの場合
		 */
		buttonInit : function(){
			var A = this;
			
			// 大きさ
			var w = this.des.width() + parseInt(this.des.css('padding-left')) + parseInt(this.des.css('padding-right'));
			var h = this.des.height() + parseInt(this.des.css('padding-top')) + parseInt(this.des.css('padding-bottom'));
			
			// ラッパのスタイル
			this.div.css({
				width : w,
				height : h,
				display : 'inline-block',
				position : 'relative',
				verticalAlign : 'middle'
			});
			
			// IE7対応
			if(!$.support.tbody){
				this.div.css({
					display : 'inline',
					zoom : 1
				});
			}
			
			// 余分なスタイルを剥がす
			this.item.css({
				position : 'absolute',
				left : 0,
				top : 0,
				width : w,
				height : h,
				margin : 0,
				padding : 0,
				borderWidth : '0px',
				opacity : 0,
				zIndex : 5,
				cursor : 'pointer'
			});
			
			// フォーム要素はラップする
			this.item.wrap(this.div);
			this.div = this.item.parent();
			
			// CSSが適用されてる要素を一旦外す
			this.des.remove();
			this.des.css({ position : 'absolute', left : 0, top : 0 });
			
			// 長体の処理
			if(!!this.isNarrow){
				this.span.html(this.item.html());
				this.des.append(this.span);
				this.div.prepend(this.des);
				this.span.strNarrow(this.des.width());
			}
			else{
				this.des.html(this.item.html());
				this.div.prepend(this.des);
			}
			
			this.des.addClass('des');
		},
		
		/**
		 * radioの場合
		 */
		radioInit : function(){
			var A = this;
			
			// 大きさ
			var w = this.des.width() + parseInt(this.des.css('padding-left')) + parseInt(this.des.css('padding-right'));
			var h = this.des.height() + parseInt(this.des.css('padding-top')) + parseInt(this.des.css('padding-bottom'));
			
			// ラッパのスタイル
			this.div.css({
				width : w,
				height : h,
				display : 'inline-block',
				position : 'relative',
				verticalAlign : 'middle'
			});
			
			// IE7対応
			if(!$.support.tbody){
				this.div.css({
					display : 'inline',
					zoom : 1
				});
			}
			
			// CSSが適用されてる要素を一旦外す
			this.des.remove();
			this.des.css({ position : 'absolute', left : 0, top : 0 });
			
			// フォーム要素はラップする
			this.item.wrap(this.div);
			this.div = this.item.parent();
			
			// 余分なスタイルを剥がす
			this.item.css({
				position : 'absolute',
				left : 0,
				top : 0,
				width : w,
				height : h,
				borderWidth : '0px',
				margin : 0,
				padding : 0,
				opacity : 0,
				zIndex : 5,
				cursor : 'pointer'
			});
			
			// CSSが適用されてる要素を再投入
			this.div.prepend(this.des);
			this.des.addClass('des');
			
			// イベントリスナを設定
			this.item.on('click',function(){
				if($(this).is(':disabled')) return;
				
				$(this).prop('checked',true);
				
				// CSSが適用されてる要素にcheckedを伝える
				A.des.addClass('checked');
				
				// 同nameのradioのチェックを外す
				$('input[name=' + $(this).attr('name') + ']').each(function(){
					if(this != A.item.get(0)){
						$(this).prop('checked',false);
						$('.des',$(this).parent()).removeClass('checked');
					}
				});
			});
			
			// 初期チェック
			if(this.item.is(':checked')) this.des.addClass('checked');
			
			if(this.item.is(':disabled')){
				this.des.addClass('disabled');
				this.item.css({
					cursor : 'default'
				});
			}
		},
		
		/**
		 * checkboxの場合
		 */
		checkInit : function(){
			var A = this;
			
			// 大きさ
			var w = this.des.width() + parseInt(this.des.css('padding-left')) + parseInt(this.des.css('padding-right'));
			var h = this.des.height() + parseInt(this.des.css('padding-top')) + parseInt(this.des.css('padding-bottom'));
			
			// ラッパのスタイル
			this.div.css({
				width : w,
				height : h,
				display : 'inline-block',
				position : 'relative',
				verticalAlign : 'middle'
			});
			
			// IE7対応
			if(!$.support.tbody){
				this.div.css({
					display : 'inline',
					zoom : 1
				});
			}
			
			// CSSが適用されてる要素を一旦外す
			this.des.remove();
			this.des.css({ position : 'absolute', left : 0, top : 0 });
			
			// フォーム要素はラップする
			this.item.wrap(this.div);
			this.div = this.item.parent();
			
			// 余分なスタイルを剥がす
			this.item.css({
				position : 'absolute',
				left : 0,
				top : 0,
				width : w,
				height : h,
				borderWidth : '0px',
				margin : 0,
				padding : 0,
				opacity : 0,
				zIndex : 5,
				cursor : 'pointer'
			});
			
			// CSSが適用されてる要素を再投入
			this.div.prepend(this.des);
			this.des.addClass('des');
			
			// イベントリスナを設定
			this.item.click(function(evt,FLAG){
				if($(this).is(':disabled')) return;
				
				// CSSが適用されてる要素にcheckedを伝える
				if($(this).is(':checked')) A.des.addClass('checked');
				else A.des.removeClass('checked');
			});
			
			// 初期チェック
			if(this.item.is(':checked')) this.des.addClass('checked');
			
			if(this.item.is(':disabled')){
				this.des.addClass('disabled');
				this.item.css({
					cursor : 'default'
				});
			}
		},
		
		/**
		 * selectの場合
		 */
		selectInit : function(){
			var A = this;
			
			// 大きさ
			var w = this.des.width() + parseInt(this.des.css('padding-left')) + parseInt(this.des.css('padding-right'));
			var h = this.des.height() + parseInt(this.des.css('padding-top')) + parseInt(this.des.css('padding-bottom'));
			var box_w = (function(){
				var ret = w;
				var ml = A.des.css('margin-left');
				var mr = A.des.css('margin-right');
				if(/\d/.test(ml)) ret += parseInt(ml);
				if(/\d/.test(mr)) ret += parseInt(mr);
				return ret
			})();
			var box_h = (function(){
				var ret = h;
				var mt = A.des.css('margin-top');
				var mb = A.des.css('margin-bottom');
				if(/\d/.test(mt)) ret += parseInt(mt);
				if(/\d/.test(mb)) ret += parseInt(mb);
				return ret;
			})();
			
			// ラッパのスタイル
			this.div.css({
				width : box_w,
				height : box_h,
				display : 'inline-block',
				position : 'relative'
			});
			
			// IE7対応
			if(!$.support.tbody){
				this.div.css({
					display : 'inline',
					zoom : 1
				});
			}
			
			// 余分なスタイルを剥がす
			this.item.css({
				position : 'absolute',
				left : 0,
				top : 0,
				height : box_h,
				width : box_w,
				fontSize : this.des.css('font-size'),
				opacity : 0,
				zIndex : 5,
				cursor : 'pointer'
			});
			
			
			
			// フォーム要素はラップする
			this.item.wrap(this.div);
			this.div = this.item.parent();
			
			// CSSが適用されてる要素を一旦外す
			this.des.remove();
			this.des.css({ position : 'absolute', left : 0, top : 0 });
			
			// 長体の処理
			if(!!this.isNarrow){
				this.span.html(this.item.get(0).options[this.item.get(0).selectedIndex].text);
				this.des.append(this.span);
				this.div.prepend(this.des);
				this.span.strNarrow(this.des.width());
			}
			else{
				this.des.html(this.item.get(0).options[this.item.get(0).selectedIndex].text);
				this.div.prepend(this.des);
			}
			
			// イベントリスナを設定
			this.item.on('change',function(evt,FLAG){
				if(A.item.get(0).selectedIndex < 0) A.item.get(0).selectedIndex = 0;
				
				var text = A.item.find('option:selected').text();
				
				if(!!A.isNarrow){
					A.span.html(text);
					A.span.strNarrow(A.des.width());
				}
				else{
					A.des.html(text);
				}
				
				// ホイールで変更されないようにフォーカスを外す
				A.item.blur();
			});
		}
	};
	
})(jQuery);





