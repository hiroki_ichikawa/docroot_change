<?php
/**
 * class
 * MacのDocumenrRootを変えるやーつ
 *
 *
 */

class DocRootChanger {

	var $data_file = null;
	var $DR_file = null;
	var $select_length = null;

	var $data = array();
	var $current_path = null;

	var $is_available = true;


	/**
	 * コンストラクタ
	 * @param {String} data_file データファイル置き場（省略可）
	 * @param {String} DR_file ドキュメントルート情報が書き込まれるファイル（省略可）
	 * @param {Number} select_length セレクトボックスの高さ（省略可）
	 */
	function DocRootChanger($data_file=null,$DR_file=null,$select_length=null){

		$this->data_file = ($data_file !== null) ? $data_file : sprintf("%s/DocumentRoot.dat.txt",dirname(dirname(dirname(__FILE__)))) ;
		$this->DR_file = ($DR_file !== null) ? $DR_file : sprintf("%s/DocumentRoot.conf.txt",dirname(dirname(dirname(__FILE__))));
		$this->select_length = ($select_length !== null) ? $select_length : 10;

		if(
			$this->data_file == null ||
			!is_file($this->data_file) ||
			$this->DR_file == null ||
			!is_file($this->DR_file) ||
			$this->select_length == null
		){
			$this->is_available = false;
			return false;
		}
	}

	/**
	 * データファイルを読んで$this->dataに格納
	 * ひとつひとつのデータは
	 * { "label" : "ラベル", "path" : "パス" }
	 * の形の配列で格納
	 */
	function readData(){
		$arr = file($this->data_file,FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		sort($arr);
		foreach($arr as $i => $dat){
			if(substr($dat,0,1) == '#') continue;
			
			$tmp = str_getcsv($dat);
			$this->data[] = array(
				"label" => $tmp[0],
				"path" => $tmp[1]
			);
		}
	}

	/**
	 * DR_fileを読んで現在のDOC_ROOTのパスを$this->current_pathに格納
	 *
	 */
	function getCurrentPath(){
		if($fp = fopen($this->DR_file,"r")){
			$buf = fread($fp,filesize($this->DR_file));
			preg_match("/DocumentRoot \"([^\"]+)\"/",$buf,$match);

			if(!empty($match[1])){
				$this->current_path = $match[1];
			}
		}
	}

	/**
	 * $this->dataから該当するパスのラベルを探して返す
	 *
	 * @param {String} path 探すパス（省略可。省略された場合は$this->current_pathを使う）
	 */
	function getLabelByPath($path=null){
		if($path == null) $path = $this->current_path;
		$ret = null;
		foreach($this->data as $i => $dat){
			if($dat["path"] == $path) $ret = $dat["label"];
		}

		return $ret;
	}

	/**
	 * 指定されたパスをDR_fileに書き込む
	 *
	 * @param {String} path 書き込むパス
	 */
	function writeDRFile($path){

		$str = sprintf("DocumentRoot \"%s\"",$path);

		if($fp = @fopen($this->DR_file,"w")){
			if(fwrite($fp,$str)){
				fclose($fp);
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}

	/**
	 * httpdを再起動する
	 *
	 */
	function httpdGraceful(){
		return shell_exec("sudo /usr/sbin/httpd -k graceful");
	}

	/**
	 * <select>要素を書く
	 *
	 */
	function writeSelectHTML(){

		$str = "";
		$str .= sprintf("<select name=\"q\" size=\"%d\">",$this->select_length);

		foreach($this->data as $i => $dat){
			$selected = ($dat["path"] == $this->current_path) ? " selected" : "" ;

			// グループ名をクラス名にする
			// グループの先頭には「group-first」をつける
			$classname = array();
			$own_class = $this->getGroupName($dat);
			$classname[] = $own_class;
			if($i != 0 && $this->getGroupName($this->data[$i-1]) != $own_class){
				$classname[] = "group-first";
			}

			$class_attr = (!empty($classname)) ? sprintf(" class=\"%s\"",implode(" ",$classname)) : "" ;

			$str .= sprintf("<option value=\"%s\"%s%s>%s</option>",$dat["path"],$class_attr,$selected,$dat["label"]);
		}

		$str .= "</select>";

		echo $str;
	}

	/**
	 * ラベルからグループ名を割り出す
	 * ラベルが「グループ名_詳細」の形になっているはずなのでグループ名を返す
	 *
	 * @param {Array} dat ラベルとパスの配列
	 */
	function getGroupName($dat){
		$ret = "";
		preg_match("/^([^_]+)/",$dat["label"],$match);
		if(!empty($match[1])) $ret = $match[1];

		return $ret;
	}
}
