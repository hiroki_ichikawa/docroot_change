<?php
require("DocRootChanger.class.php");

$drc = new DocRootChanger();
if(!$drc->is_available) exit;

$drc->readData();
$error = array();

if(isset($_POST["q"])){
	$new_path = $_POST["q"];
	if($drc->writeDRFile($new_path)){
		if($mes = ($drc->httpdGraceful()) != ""){
			$error[] = "httpd gracefulに失敗しました : " . $mes;
		}
		else{
			$drc->current_path = $new_path;
		}
	}
	else{
		$error[] = "DRファイルの書き込みに失敗しました";
	}
}

$drc->getCurrentPath();

?>
<!DOCTYPE html>
<html dir="ltr" lang="ja-JP">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<title>MacのApacheのDOCROOTを変更するやーつ</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" type="text/css" href="_css/reset.css" media="screen,print">
<link rel="stylesheet" type="text/css" href="_css/standard.css" media="screen,print">
<script src="_javascript/jquery-1.11.1.min.js"></script>
<script src="_javascript/jquery.designedform.js"></script>
<script>
$(function(){
	$('.designed').designedForm();
});

$(window).on('blur',function(){
	window.close();
});

$(function(){
	$('select').focus();
});

$(document).on('keyup',function(evt){
	if(evt.keyCode == 13) $('form').submit();
});

</script>
<body>
<h1>MacのApacheのDOCROOTを変更するやーつ</h1>
<div id="wrap">

<?php if(isset($_POST["q"])){ ?>
<?php if(empty($error)){ ?>

<div class="success">
<h2>DOCUMENT_ROOTの変更に成功しました</h2>
</div>

<?php } else { ?>

<div class="error">
<h2>DOCUMENT_ROOTの変更に失敗しました</h2>
<ul>
<?php foreach($error as $i => $mes){ printf("<li>%s</li>",$mes); } ?>
</ul>
</div>

<?php } ?>
<?php } ?>

<div class="current">
<p>現在のDOCUMENT_ROOTは</p>
<p><span><em><?php echo $drc->getLabelByPath(); ?></em></span></p>
<p class="path"><?php echo $drc->current_path; ?></p>
</div>


<form method="post" action="./index.php">
<p><?php $drc->writeSelectHTML(); ?></p>
<p><input type="submit" value="変更" class="designed" rel="submit-button"></p>
	<div id="submit-button"></div>
</form>


</div>
</body>

</html>
